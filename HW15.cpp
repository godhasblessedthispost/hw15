﻿#include <iostream>

const int N = 6;
int a = 0;

int main()
{
    // only even = 0 / only odd = 1
    int EvenOdd = 0;

    switch (EvenOdd)
    {
    case 0:
        for (; a <= N; a += 2)
        {
            std::cout << a << "\n";
        }
        break;
    case 1:
        std::cout << a << "\n";
        a += 1;
        for (; a <= N; a += 2)
        {
            std::cout << a << "\n";
        }
    }
}

/*
// Эта версия короче, но в ней не выводится ноль если выводить только нечётные числа.
int main()
{
    int N = 6;

    // only even a = 0 / only odd a = 1
    for (int a = 0; a <= N; a += 2)
    {
        std::cout << a << "\n";
    }
}
*/